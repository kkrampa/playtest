name := """just-play-java"""

version := "1.0-SNAPSHOT"

play.Project.playJavaSettings

libraryDependencies ++= Seq(
  "org.postgresql" % "postgresql" % "9.3-1100-jdbc4",
  "org.apache.directory.studio" % "org.apache.commons.io" % "2.4",
  javaJdbc,
  javaEbean
)