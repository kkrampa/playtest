package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import play.db.ebean.Model;

@Entity
public class Image extends Model{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6727022222330879650L;

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String name;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public static Finder<Long,Image> find = new Finder<Long,Image>(
		    Long.class, Image.class
	); 
}
