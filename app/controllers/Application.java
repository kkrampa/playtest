package controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import models.Image;

import org.apache.commons.io.FileUtils;

import play.Play;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;

public class Application extends Controller {

  public static Result index()  {
	  List<Image> obrazki = Image.find.all();
	  return ok(views.html.main.render(obrazki));
  }
  
  public static Result save() {
	  MultipartFormData body = request().body().asMultipartFormData();
	  FilePart picture = body.getFile("photo");
	  if (picture != null) {
	    String fileName = picture.getFilename();
	    //String contentType = picture.getContentType(); 
	    File file = picture.getFile();
	    try {
			FileUtils.moveFile(file, new File(Play.application().configuration().getString("upload.path"),
					fileName));
			Image image = new Image();
			image.setName(fileName);
			image.save();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    flash("success", "Upload zakończony pomyślnie");
	    return redirect(routes.Application.index());
	  } else {
	    flash("error", "Wystąpił błąd podczas uploadu");
	    return redirect(routes.Application.index());    
	  }
  }
  
  public static Result renderImage(String name) {
	Path path = Paths.get(Play.application().configuration().getString("upload.path"), name);
	byte[] data = null;
	try {
		data = Files.readAllBytes(path);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return ok(data).as("jpeg");
	  
  }

}
